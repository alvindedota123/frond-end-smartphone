const fetchPost = () => {
  return fetch('http://localhost:3000/smartphones', {
    next: {
      revalidate: 60
    }
  }).then((res) =>
    res.json()
  )
}
export default async function VerPage () {
  const posts = await fetchPost()
  return (
    <>
      <table>
        <thead>
          <tr>
            <th>ID</th>
            <th>Nombre</th>
            <th>Modelo</th>
            <th>Precio Referencial</th>
            <th>Precio Venta</th>
            <th>Año Modelo</th>
            <th>Fecha de Creación</th>
            <th>Fecha de Actualización</th>
          </tr>
        </thead>
        <tbody>
          {posts.map((post) => (
            <tr key={post.id}>
              <td>{post.id}</td>
              <td>{post.nombre}</td>
              <td>{post.modelo}</td>
              <td>{post.precio_referencial}</td>
              <td>{post.precio_venta}</td>
              <td>{post.ano_modelo}</td>
              <td>{post.created_at}</td>
              <td>{post.updated_at}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </>
  )
}
