'use client'

import React, { useState } from 'react'

function CrearPage () {
  const [formData, setFormData] = useState({
    id: '',
    nombre: '',
    modelo: '',
    precio_referencial: '',
    precio_venta: '',
    ano_modelo: ''
  })

  const handleChange = (event) => {
    const { name, value } = event.target
    setFormData((prevData) => ({
      ...prevData,
      [name]: value
    }))
  }

  const handleSubmit = async (event) => {
    event.preventDefault()
    try {
      const response = await fetch('http://localhost:3000/smartphones', {
        method: 'POST',
        headers: {
          'Content-Type': 'application/json'
        },
        body: JSON.stringify(formData)
      })
      if (response.ok) {
        // Procesar respuesta exitosa
        console.log('Datos enviados correctamente')
      } else {
        // Procesar respuesta con error
        console.error('Error al enviar los datos')
      }
    } catch (error) {
      console.error('Error de red:', error)
    }
  }

  return (
    <form onSubmit={handleSubmit}>
      <div>
        <label htmlFor='id'>ID:</label>
        <input type='text' id='id' name='id' value={formData.id} onChange={handleChange} disabled />
      </div>

      <div>
        <label htmlFor='nombre'>Nombre:</label>
        <input type='text' id='nombre' name='nombre' value={formData.nombre} onChange={handleChange} />
      </div>

      <div>
        <label htmlFor='modelo'>Modelo:</label>
        <input type='text' id='modelo' name='modelo' value={formData.modelo} onChange={handleChange} />
      </div>

      <div>
        <label htmlFor='precio_referencial'>Precio Referencial:</label>
        <input
          type='number'
          id='precio_referencial'
          name='precio_referencial'
          value={formData.precio_referencial}
          onChange={handleChange}
        />
      </div>

      <div>
        <label htmlFor='precio_venta'>Precio Venta:</label>
        <input
          type='number'
          id='precio_venta'
          name='precio_venta'
          value={formData.precio_venta}
          onChange={handleChange}
        />
      </div>

      <div>
        <label htmlFor='ano_modelo'>Año Modelo:</label>
        <input type='number' id='ano_modelo' name='ano_modelo' value={formData.ano_modelo} onChange={handleChange} />
      </div>

      <input type='submit' value='Enviar' />
    </form>
  )
}

export default CrearPage
