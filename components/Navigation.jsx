import Link from 'next/link'
import styles from './Navigation.module.css'
const links = [
  {
    label: 'Home',
    route: '/'
  },
  {
    label: 'Crear',
    route: '/crear'
  },
  {
    label: 'Ver',
    route: '/ver'
  },
  {
    label: 'Modificar',
    route: '/modificar'
  },
  {
    label: 'Eliminar',
    route: '/eliminar'
  }

]

export function Navigation () {
  return (
    <header className={styles.header}>
      <nav>
        <ul className={styles.navigation}>
          {links.map(({ label, route }) => (
            <li key={route} className={styles.li}>
              <Link href={route}>{label}</Link>
            </li>
          ))}
        </ul>
      </nav>
    </header>
  )
}
